

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        
        let user = username.text!
        let pass = password.text! //El signo de interrogacion sirve para evitar los errores de nulos
        
        Auth.auth().signIn(withEmail: user, password: pass) { (data, error) in
            
            if let err = error { // Si error es null el if falla es falso
                print (err)
                return
            }
            
            
            // Esta linea ejecuta la transicion con que identificador
            // El self hace referencia a la clase principal al view controller.
            self.performSegue(withIdentifier: "loginSegue", sender: self )
        }
         username.text = ""
        password.text = ""
        }
    
    
}

