
import UIKit
import Alamofire
import AlamofireObjectMapper
import FirebaseDatabase



class WeatherLogged: UIViewController, UITableViewDelegate,UITableViewDataSource  {

    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        //getFireBaseinfo() se lo ubica mejor cada que aparezca la pantalla -> willAppear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getFireBaseinfo()
    }
    
    
    //Se eliminan todos los observers de esa referencia
    override func viewWillDisappear(_ animated: Bool) {
        ref.removeAllObservers()
    }
    
    //Para configurar una tabla se necesita: 1)informacion de secciones,
    // 2) numero de filas de la seccion
    // 3) informacion de cada fila
    // Data Source(informacion de la tabla) y Delegate(Visual)
    // control+clic y halar al botón amarillo dos veces(Datasource y Delegate). Además, hay que definir en la clase que es delegate y Datasource
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var citiesTableview: UITableView!
    
    var ref:DatabaseReference!
    var weathers:[[String:String]] = []  //Es un arreglo de diccionarios que se inicializa en vacio
   var selectedIndex = 0
    
    @IBAction func getWeather(_ sender: Any) {

       let URL = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text  ?? "quito")&appid=27563ff0022584f3baa9304aef7d92ba";
       
        Alamofire.request(URL).responseObject{(response: DataResponse<weather>)
            in
            print(response.value?.weather?[0].weather);
            
            self.weatherLabel.text = response.value?.weather?[0].weather ?? ""
            
            
            let uuid = UUID().uuidString
            self.ref.child("weather").child(uuid).setValue([
                "city": self.cityTextField.text ?? "n/a",
                "weather": response.value?.weather?[0].weather ?? ""
                ])
        }
    }
    
    @IBAction func goLogin(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goGeoWeather(_ sender: Any) {
        self.performSegue(withIdentifier: "geoSegue", sender: self )
        cityTextField.text = ""
    }
    
    func getFireBaseinfo(){
        let weatherRef = self.ref.child("weather")
        weatherRef.observe(.childAdded){(dataSnapshot) in  //escucha lo que se va añadiendo (.childAdded)
            //print(dataSnapshot)
            //as realiza casteos
            var weatherDict = dataSnapshot.value as? [String:String] ?? [:]
            //obtener el id
            weatherDict["id"] = dataSnapshot.key
            
            self.weathers += [weatherDict]
            print(self.weathers)
            self.citiesTableview.reloadData() //La tabla es refrescada para mostrar ahora sì el contenido de la tabla (Para consultas asincronas y API's)
        }
        
        //Eliminar el item, la funcion de reloadData se elimina para optimizar y se lo ubica en el willAppear. Se corrige manteniendo esta funcion pero el getFireBaseinfo va en el willAppear.
        weatherRef.observe(.childRemoved){(dataSnapshot) in
            let id = dataSnapshot.key
            self.weathers =   self.weathers.filter(){ $0["id"] != id}
            self.citiesTableview.reloadData()
        }
        
    }
    
    //TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let cell = UITableViewCell()
        //cell.textLabel?.text = "\(indexPath)"
        let cityname = weathers[indexPath.row]["city"] ?? "city"
        let weather = weathers[indexPath.row]["weather"] ?? "weather"
        cell.textLabel?.text = "\(cityname) - \(weather)"
        return cell
 */
        
        // Sección de la TableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell") as! WeatherTableViewCell
        
        cell.cityLabel.text = weathers[indexPath.row]["city"] ?? "city"
        cell.weatherLabel.text = weathers[indexPath.row]["weather"] ?? "weather"
        return cell
        
        
    }
    
    //Funcion para cambiar el indice que es seleccionado en la tabla
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedIndex = indexPath.row
        return indexPath
    }
    
    //Método que se ejecuta en transiciones, tiene infomración de la clase actual y de la clase destino
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! InfoViewController
        destination.weatherDict = weathers[selectedIndex]
    }
    
}
