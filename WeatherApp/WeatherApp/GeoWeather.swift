//
//  GeoWeather.swift
//  weatherApp
//
//  Created by Steven Cuasqui on 11/13/18.
//  Copyright © 2018 Jose Azadobay. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class GeoWeather: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    @IBAction func goWeatherMain(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func setCoordinates(){
        latitude.text = "51.5073219"
        longitude.text = "-0.1276474"
        
    }
    
    
    @IBOutlet weak var latitude: UITextField!
    @IBOutlet weak var longitude: UITextField!
    @IBOutlet weak var weatherGeoLabel: UILabel!
    
    @IBAction func generateCoord(_ sender: Any) {
        setCoordinates()
    }
    
    @IBAction func getGeoWeather(_ sender: Any) {
        let URL = "http://api.openweathermap.org/data/2.5/weather?appid=7f20821277c0093e1f8777a45ab56598&lat=\(latitude.text  ?? "40.7127837")&lon=\(latitude.text  ?? "-74.0059413")";
        
        //Pasar 2 variables con latitud y ..... coordenadas geograficas una pantalla mas.
        //Completar la otra pantalla
        //Poner botones para regresar
        
        Alamofire.request(URL).responseObject{(response: DataResponse<weatherGeo>)
            in
            print(response.value?.weather?[0].weather);
            
            self.weatherGeoLabel.text = response.value?.weather?[0].weather ?? ""
        }
        
        print("Estas son las coordenadas:",latitude.text,longitude.text)
    }
    

}
