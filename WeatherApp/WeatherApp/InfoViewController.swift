//
//  InfoViewController.swift
//  weatherApp
//
//  Created by Steven Cuasqui on 12/14/18.
//  Copyright © 2018 Jose Azadobay. All rights reserved.
//

import UIKit
import FirebaseDatabase

class InfoViewController: UIViewController {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    
    var weatherDict:[String:String]?
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityLabel.text = weatherDict?["city"] ?? "city"
        weatherLabel.text = weatherDict?["weather"] ?? "weather"
        ref = Database.database().reference().child("weather")
    }

    @IBAction func removeButtonPressed(_ sender: Any) {
        
        ref.child(weatherDict?["id"] ?? "cosas").removeValue()
        navigationController?.popViewController(animated:true)
    }
}
