

import Foundation
import ObjectMapper

class weather: Mappable {
    
    var weather: [SingleWeather]?
    var weatherId: Int?
    var name: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        weather <- map["weather"]
        weatherId <- map["id"]
        name <- map["name"]
        
    }
    
}


class SingleWeather: Mappable{
    var weather: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        weather <- map["main"]
    }
    
}

class weatherGeo: Mappable {
    
    var weather: [SingleWeather]?
    var weatherId: Int?
    var name: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        weather <- map["weather"]
        weatherId <- map["id"]
        name <- map["name"]
        
    }
    
}
